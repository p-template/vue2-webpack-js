module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: '@import "./src/styles/init.scss";',
      },
    },
    sourceMap: true,
  },
  devServer: {
    progress: false,
    port: 7010,
    overlay: {
      warnings: false,
      errors: true,
    },
  },
};
