import request from '@/api';

const fakePermission = [
  {
    parentCode: 'A',
    subCode: 'A-1',
    permission: 4,
  },
  {
    parentCode: 'A',
    subCode: 'A-2',
    permission: 7,
  },
  {
    parentCode: 'B',
    subCode: 'B-1',
    permission: 7,
  },
  {
    parentCode: 'B',
    subCode: 'B-2',
    permission: 7,
  },
  {
    parentCode: 'B',
    subCode: 'B-3',
    permission: 7,
  },
  {
    parentCode: 'S',
    subCode: 'S-1',
    permission: 7,
  },
  {
    parentCode: 'S',
    subCode: 'S-2',
    permission: 7,
  },
];

export const getUsersAPI = async () => {
  const res = await request({ method: 'get', url: '/users/1' });
  return res.data;
};

export const getPermissionAPI = async () => {
  return new Promise((res) => {
    setTimeout(() => {
      res(fakePermission);
    }, 200);
  });
};
