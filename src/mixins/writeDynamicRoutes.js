import genMenu from '@/router/genMenu';
import { routeObservable, routeMutation } from '@/observable/route';
import router from '@/router';

import { getPermissionAPI } from '@/api/test';

const page404route = {
  path: '*',
  name: 'page404',
  component: () => import(/* webpackChunkName: "Page404" */ '@/views/Page404.vue'),
  meta: {
    title: '404',
  },
};

export default {
  computed: {
    isWriteDynamicRoutes() {
      return routeObservable.isWriteDynamicRoutes;
    },
  },
  methods: {
    setMenuList: routeMutation.setMenuList,
    async writeDynamicRoutes(from) {
      // 當前頁面為包括在下列陣列內時，且來源為app時不執行
      const dontExecPagesFromApp = ['login'];
      if (from === 'app' && dontExecPagesFromApp.includes(this.$route.name)) return;
      if (this.isWriteDynamicRoutes) return;

      routeMutation.setIsWriteDynamicRoutes();

      const token = true;
      if (token) {
        try {
          const authList = await getPermissionAPI();
          const menuList = genMenu(authList);
          console.log('menuList', menuList);
          this.setMenuList(menuList);
        } catch (e) {
          console.log('e...', e);
        }
      }

      // 404 page 必須在routes最後面
      router.addRoute(page404route);
    },
    // },
  },
};
