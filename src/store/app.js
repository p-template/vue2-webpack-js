// 引入type，避免function衝突
import * as type from '@/store/types';

export default {
  namespaced: true,
  state: {
    count: 0,
  },
  getters: {
    multiCount({ count }) {
      return count * 2;
    },
  },
  mutations: {
    [type.mutateCount](state, payload) {
      console.log('hi~');
      state.count = state.count + 1;
    },
  },
  actions: {
    [type.actionCount]({ commit }, payload) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          commit('mutateCount');
          resolve();
        }, 1000);
      });
    },
  },
};
